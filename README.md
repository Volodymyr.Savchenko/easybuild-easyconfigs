|  |  |
| ------ | ------ |
| **Prepared By** | VS |
| **Prepared For** | Astro Department IT team |
| **See also** | [general notes on common software](https://www.astro.unige.ch/wiki/Notes%20on%20Common%20HEA%20Astro%20Software%20Management) |
|  | [relevant HPC lunch](https://hpc-community.unige.ch/t/save-the-date-hpc-lunch-autumn-session/1044) |
|  | [a PR for publishing an easyconfig upsteam](https://github.com/easybuilders/easybuild-easyconfigs/pull/12835) |
|  | [discussion with Dominique Eckert](https://hpc-community.unige.ch/t/creating-modules-for-heasoft-and-xmmsas/1710/3) |
|  | [discussion with D. Eckert about x11](https://hpc-community.unige.ch/t/cannot-find-x11-intrinsic-h/1484) |

# Introduction

This repository contains easybuild recipies OSA and HEASoft, with dependency on python. And some common notes for easybuild on HPC at Astro and UNIGE.



# Proposed/pre-agreed plan

1. develop EB files customly in own directory
2. install common astro software in self-managed common astro directory (note shared spaces /srv/beegfs/scratch/shares/astro/ and /home/share/astro), see [note on shared astro partition](https://gitlab.unige.ch/Volodymyr.Savchenko/doc-common-astro-partition)
3. (_optional, especially if needed for broader user group_) 
 provide eb file to upstream (if accepted by the upsteam): https://github.com/easybuilders/easybuild-easyconfigs
4. (_very optional, especially if needed for much broader (global) user group_ ) 
provide eb file to less strict upstream of UNIGE HPC

building should be done on a compute node. It is not clear on which one. Seems like baobab nodes work better for this, due to different ssl libraries, found by easybuild osdependencies. But yggdrasil should be compatible. To be verified.

# EasyBuild vs Containers

As of now, INTEGRAL software (primarily OSA and it's dependency - HEASoft) is built, tested, and packaged in containers (see e.g. https://gitlab.astro.unige.ch/integral/build/osa-build).

EasyBuild encompases the build procedure and can be used inside the container, instead of a more custom script used currently.

The same EasyConfig can be then used to build and install this software on a HPC system.

One may consider taht EasyBuild does all the same job as container-based workflow. However, containers are critical for distribution in environments other than HPC clusters with well-developed homogenous enviuonment.
The same containers, as singularity, can be used on HPC, when the necessary modules can not be feasibly built.

See example http://www.hpcadvisorycouncil.com/events/2017/swiss-workshop/pdf/Tuesday11April/GPezzi.EasyBuildonLargeScaleSyst_Tue.04112017.pdf

## TODO

Software can be stored on git or from official source code locations.
CI/CD can be used to test the eb files, and possibly also to deploy them.

## Status

After several discussions with UNIGE HPC team (2020 - early 2021), the preferred approach for installing shared software is clarified and explained in this note.

INTEGRAL used some of the installations. However, INTEGRAL relies on singularity containers - there is no advantage of re-building again. 

It is possible other people will need it, see [request](https://hpc-community.unige.ch/t/creating-modules-for-heasoft-and-xmmsas/1710/4).

## On sourcing arbitrary bash scripts

Sourcing scripts from within module files is not supported, since it can not be easily reversed.
See also a [relevant issue](https://github.com/easybuilders/easybuild-framework/issues/3275)

Instead, we can deduce the variables to be set automatically, and add them in the easyconfig (see example of HEASoft).


