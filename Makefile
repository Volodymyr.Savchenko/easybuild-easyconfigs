ASTRO_BASE?=/home/share/astro/easybuild/
#ASTRO_BASE?=/home/share/astro/test-eb-savchenk/

SHELL:=bash

HEASOFT_VERSION?=6.29
OSA_VERSION?=11.1-13-g4c4052b6-20201019-182542

install:
	echo "running on $(shell hostname) at $(shell date)"
	module load EasyBuild/4.4.2; eb $(EB)  \
	    	--configfiles $(PWD)/config.cfg \
		--devel \
		--debug \
		-r . $(EXTRA)

osa:
	echo "running on $(shell hostname) at $(shell date)"
	module load EasyBuild/4.4.2 && eb OSA-$(OSA_VERSION)-GCCcore-9.3.0.eb  \
	    	--configfiles $(PWD)/config.cfg \
		-r . $(EXTRA)

    #--installpath-software=$(ASTRO_BASE)/ebsofts \
    #--installpath-modules=$(ASTRO_BASE)/ebmodules

heasoft:
	echo "running on $(shell hostname) at $(shell date)"
	module load EasyBuild/4.4.2; eb HEASoft-6.29-GCCcore-9.3.0.eb  \
    		--configfiles $(PWD)/config.cfg \
		--robot \
		-r . $(EXTRA)

eb-update-pr:
	eb  --update-pr 12835 HEASoft-6.27.2-GCCcore-4.9.3.eb   --pr-commit-msg "$(MESSAGE)" --github-user volodymyrss
